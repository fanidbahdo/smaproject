/**
 * Entry point for this project.
 *
 * @author Nid-bahdou et El meskine
 */

public class Gazelle implements Animal {
	
    // **************************************************
    // Attributs
    // **************************************************
	
    private double probaReduce;
    ///lE NOM DE L'ANIMAL
    private String nom="gazelle";
    protected Plot emplacement =new Plot();;
    protected int gender;//1 pour female  0 pour male
    protected int age=0;
    protected int G_or_L;//1 si lion 2 si gazelle
    protected int yn=0;
    protected Double energie=100.0;
    protected int gain_from_food=0;

    // **************************************************
    // Constructors
    // **************************************************

	/*!
	 * Default constructor.
	 */
    
	Gazelle()
	{
		probaReduce=0.04;
    	double r=Math.random();
    	
    	if(r<0.5)
    	{
    		gender=1;
    	}
    	else
    	{
            gender=0;	    		
    	}
    }
	
	/**
	 * Combien il a gagne de la nourriture 
	 */
	///OUTPUT : gain_from_food
    public int getGain_from_food() 
    {
		return gain_from_food;
	}

	/**
	 * Setter pour combien il a gagne de la nourriture 
	 */
	public void setGain_from_food() 
	{
		this.gain_from_food = this.gain_from_food+10;
	}

	public int getYn() 
	{
		return yn;
	}

	public void setYn(int yn)
	{
		this.yn = yn;
	}

	/**
	 * Pour savoir si male ou femelle
	 */
	///OUTPUT : Gazelle ou Lion
	public int getG_or_L() 
	{
		return G_or_L;
	}

	/**
	 * Poser si male ou bien femelle
	 */
	///INPUT : Gazelle ou Lion
	public void setG_or_L(int g_or_L) 
	{
		G_or_L = g_or_L;
	}

	/**
	 * Pour savoir l'emplacement de la gazelle dans le plateau de jeu
	 */
	///OUTPUT : emplacement de type Plot
	public Plot getEmplacement() 
	{
		return emplacement;
	}

	/**
	 * Pour poser l'emplacement de la gazelle dans le plateau de jeu
	 */
	///INPUT : emplacement de type Plot
	public void setEmplacement(Plot emplacement) 
	{
		this.emplacement = emplacement;
	}

	/**
	 * Getter for the gender
	 */
	///OUTPUT : the gender
	public int getGender() 
	{
		return gender;
	}

	/**
	 * Setter for the gender
	 */
	///INPUT : the gender
	public void setGender(int gender)
	{
		this.gender = gender;
	}

	/**
	 * Getter pour l'age 
	 */
	///OUTPUT : l'age
	public int getAge() 
	{
		return age;
	}

	/**
	 * Incrementer l'age
	 */
	public void IncrementerAge()
	{
		this.age = this.age+1;
	}
	
	/**
	 * Combien il a consom� d'energie
	 */
	public void consomeEnergy()
    {
     energie=energie-10;
    }

	/**
	 * Pour se positionner
	 */
    public void Sepositionner()
    {
		int xf=(int)(Math.random()*10);
		int yf=(int)(Math.random()*10);
		
		emplacement.setX(xf);
		emplacement.setY(yf);

    }
	
}
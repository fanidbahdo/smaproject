/**
 * Entry point for this project.
 *
 * @author Nid-bahdou et El meskine
 */

public class Plot {
    private boolean herbeDisponible=true;//disponibilit� de l'herbe dans la case
    private int tempsRepousse=30;//temps de repousse de l'herbre
    private int x;//la coordonn�e x
    private int y;//la coordonn�e y
    private boolean b=false;//case occup� ou non
    private boolean rempliHerbe=false;//la case est-elle rempli d'herbre ou non
    private Animal animal=null;
    private String nomAnimal;//nom de l'animal
    
	/*!
	 * Default constructor.
	 */
	Plot()
	{
			
	}
	
	/**
	 * le nom de l'animal qui est dans le plot
	 */
	///OUTPUT : Le nom d'animal
	public String getNomAnimal() 
	{
		return nomAnimal;
	}

	/**
	 * savoir s'il y'a de l'herbre de ce plot
	 */
	///OUTPUT : BOOLEAN si l'herbre disponible ou non
	public boolean isHerbeDisponible() 
	{
		return herbeDisponible;
	}
	
	/**
	 * herbre disponible ou nn
	 */
	///INPUT : BOOLEAN si l'herbre disponible ou non
	public void setHerbeDisponible(boolean herbeDisponible)
	{
		this.herbeDisponible = herbeDisponible;
	}
	
	/**
	 * lion ou gazelle
	 */
	///INPUT : string du nom de l'animal
	public void setNomAnimal(String nomAnimal) 
	{
		this.nomAnimal = nomAnimal;
	}
	
	/**
	 * Getter pour l'animal dans la case
	 */
	///OUTPUT : STRING du nom de l'animal
	public Animal getAnimal() 
	{
		return animal;
	}
    
	/**
	 * Setter pour l'animal dans la case 
	 */
	///INPUT : Animal
	public void setAnimal(Animal animal) 
	{
		this.animal = animal;
	}

	
	/**
	 * Getter pour savoir si la case est rempli d'herbe ou non 
	 */
	///OUTPUT : BOOLEAN si la case est rempli d'herbre ou non
	public boolean isRempliHerbe() 
	{
		return rempliHerbe;
	}

	/**
	 * Setter pour savoir si la case est rempli d'herbe ou non
	 */
	///INPUT : BOOLEAN si la case est rempli d'herbre ou non
	public void setRempliHerbe(boolean rempliHerbe) 
	{
		this.rempliHerbe = rempliHerbe;
	}

	/**
	 * Pour savoir s'il y a un animal dans la case 
	 */	
	///OUTPUT : BOOLEAN si la case est rempli d'animal
	public boolean isB() 
	{
		return b;
	}
	
	/**
	 * Pour dire s'il y'a un animal dans la case ou non
	 */
	///INPUT : BOOLEAN si la case est rempli d'animal
	public void setB(boolean b) 
	{
		this.b = b;
	}

	/**
	 * Getter pour la coordonn�e X
	 */
	///OUTPUT : Int, La coordonn�e X
	public int getX() 
	{
		return x;
	}
	
	/**
	 * Setter pour la coordonn�e X
	 */
	///INPUT : Int, La coordonn�e X
	public void setX(int x) 
	{
		this.x = x;
	}
	
	/**
	 * Getter pour la coordonn�e Y
	 */
	///OUTPUT : Int, La coordonn�e Y
	public int getY() 
	{
		return y;
	}
	
	/**
	 * Setter pour la coordonn�e Y
	 */
	///INPUT : Int, La coordonn�e Y
	public void setY(int y)
	{
		this.y = y;
	}
	public void grow()
	{
		herbeDisponible=true;
	}
	public void eaten() //ajout�
	{
		herbeDisponible=false;
	}
}

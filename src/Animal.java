/**
 * Entry point for this project.
 *
 * @author Nid-bahdou et El meskine
 */

interface Animal {
	    ///Combien il a gagn� quand il a mang� 
	    public int getGain_from_food();
		public void setGain_from_food();
		public int getYn();
		public void setYn(int yn);
		///Lion ou Gazelle
		public int getG_or_L();
		public void setG_or_L(int g_or_L);	  
		public Plot getEmplacement();
		public void setEmplacement(Plot emplacement);
		public int getGender();
		public void setGender(int gender);
		public int getAge();
		public void IncrementerAge();
		public void consomeEnergy();
	    public void Sepositionner();

}

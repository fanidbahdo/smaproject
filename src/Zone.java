/**
 * Entry point for this project.
 *
 * @author Nid-bahdou et El meskine
 */

public interface Zone {
     public void showZoneName();
}

/**
 * Entry point for this project.
 *
 * @author Nid-bahdou et El meskine
 */

public class ZoneNonAccedee implements Zone{

	@Override
	public void showZoneName() {
		System.out.println("Je suis une zone non acced�e par les animaux");
	}
	
}

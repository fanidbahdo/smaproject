/**
 * Entry point for this project.
 *
 * @author Nid-bahdou et El meskine
 */
/**
 * Impl�mentation simple d'un singleton.
 * L'instance est cr��e � l'initialisation. 
 */
public class SingletonGame
{   
    /** Instance unique pr�-initialis�e */
    private static SingletonGame INSTANCE = new SingletonGame();
    private Plateau plateau=new Plateau();
	
    /** Constructeur priv� */
    private SingletonGame()
    {}
      
    /** Point d'acc�s pour l'instance unique du singleton */
    public static SingletonGame getInstance()
    {   return INSTANCE;
    }
    
    /** Point d'acc�s pour l'objet plateau */
    public Plateau getPlateau()
    {
    	return plateau;
    }
    
    /** setter de plateau */    
    public void setPlateau(Plateau plateau)
    {
    	this.plateau=plateau;
    }
}
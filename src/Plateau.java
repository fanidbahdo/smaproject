/**
 * Entry point for this project.
 *
 * @author Nid-bahdou et El meskine
 */ 

import java.awt.List;
import java.util.ArrayList;

public class Plateau implements Zone {
	
	private Plot p[][];
	private ArrayList<Zone> zones = new ArrayList<Zone>(); //Un plateau est compos� de plusieurs zones 
	private ArrayList<Animal> animals= new ArrayList<Animal>();// Les animaux dans le plateau
	private int nombreAnimals;//nombre d'animals dans le plateau
	private String NameZone;

	/*!
	 * Default constructor.
	 */
	public Plateau()
	{
		super();
		p = new Plot[10][10];
		int ii,jj;
		int i,j;
		
		for(ii=0;ii<10;ii++)
		{
			for(jj=0;jj<10;jj++)
			{
				p[ii][jj]=new Plot();
			}
		}
		
		
		for(i=0;i<10;i++)
		{
			for(j=0;j<10;j++)
			{
				p[i][j].setB(false);
			}
		}
		
	    //remplissage d'herbe
		for(i=0;i<60;i++)
		{
			int xx,yy;
			do {
				   xx=(int)(Math.random()*10);
				   yy=(int)(Math.random()*10);
			   }while(p[xx][yy].isRempliHerbe()==true);
			
			p[xx][yy].setRempliHerbe(true);
		}
	}
	
	
	/**
	 * Le plateau est un ensemble de zones, il prend en parametres une zone
	 */
	///INPUT : zone
	public void ajouterZone(Zone zone)
	{
		zones.add(zone);
	}
	
	/**
	 * Affiche le nom de la zone
	 */
    public void showZoneName()
    {
    	System.out.println("C'est la zone "+NameZone);
    }
    
	/**
	 * Getter pour la zone
	 */
    ///OUTPUT : le nom de la zone
	public String getNameZone() 
	{
		return NameZone;
	}

	/**
	 * Setter pour la zone
	 */
	///INPUT : Le nom de la zone
	public void setNameZone(String nameZone) 
	{
		NameZone = nameZone;
	}

	/**
	 * getter pour le plateau
	 */
	///OUTPUT : une matrice de dimension 2 qui signifie le plateau de jeu
	public Plot[][] getP() 
	{
		return p;
	}

	/**
	 * setter pour le plateau
	 */
	///INPUT : une matrice de dimension 2 qui signifie le plateau de jeu
	public void setP(Plot[][] p) 
	{
		this.p = p;
	}

	/**
	 * Ajouter un animal
	 */
	///INPUT : Un animal
	public void ajouterAnimal(Animal animal)
	{   		
		int x,y;
		x=animal.getEmplacement().getX();
		y=animal.getEmplacement().getY();
		p[x][y].setB(true);
		p[x][y].setAnimal(animal);
		this.animals.add(animal);
		this.nombreAnimals=animals.size();
	}
	
	
	/**
	 * Nombre d'animals
	 */
	///INPUT : une matrice de dimension 2 qui signifie le plateau de jeu
	public int getNombreAnimals() {
		return nombreAnimals;
	}

	/**
	 * setter pour nombre d'animal
	 */
	///INPUT : Nombre d'animals
	public void setNombreAnimals(int nombreAnimals) {
		this.nombreAnimals = nombreAnimals;
	}


	/**
	 * voir le terrain de jeu
	 */
	public void VoirJeu()
	{ 
		int i,j;
		for(i=0;i<10;i++)
		{
			for(j=0;j<10;j++)
			{   
				System.out.print(".");
				if(p[i][j].isB()==false)
				{
					System.out.print(" ");
				} 
				else if(p[i][j].getAnimal().getG_or_L()==1)
				{
					System.out.print("L");					
				}
				else if(p[i][j].getAnimal().getG_or_L()==2)
				{
					System.out.print("G");					
				}
			}
			System.out.println("");	
		}
		
	}
	
	/**
	 * Les animeaux bougent
	 */
	///INPUT : index de l'animal dans le ArrayList animals
	public void bouger(int index)
	{ 
	    
		// pour bouger 1 vers la droite 2 vers la gauche 3 vers le haut 4 vers le bas
		boolean c=false;
		int xx=0;
		while(c==false)
		{   c=true;
		    xx=(int)(Math.random()*4)+1;
		    try {
		    	if(animals.get(index).getEmplacement().getX()==0 && animals.get(index).getEmplacement().getY()==9 && (xx==1 || xx==3) )
	    		{
	    			c=false;
	    		}
	    	
	    		if(animals.get(index).getEmplacement().getX()==0 && animals.get(index).getEmplacement().getY()==0 && (xx==2 || xx==3) )
	    		{
	    			c=false;
	    		}
	    		
	    		if(animals.get(index).getEmplacement().getX()==9 && animals.get(index).getEmplacement().getY()==0 && (xx==2 || xx==4) )
	    		{
	    			c=false;
	    		}
	    		
	    		if(animals.get(index).getEmplacement().getX()==9 && animals.get(index).getEmplacement().getY()==9 && (xx==1 || xx==4) )
	    		{
	    			c=false;
	    		}
	    		
	    		if(animals.get(index).getEmplacement().getX()==0 && xx==3 )
	    		{
	    			c=false;
	    		}
	    		
	    		if(animals.get(index).getEmplacement().getX()==9 && xx==4 )
	    		{
	    			c=false;
	    		}
	    		
	    		if(animals.get(index).getEmplacement().getY()==0 && xx==2 )
	    		{
	    			c=false;
	    		}
	    		if(animals.get(index).getEmplacement().getY()==9 && xx==1 )
	    		{
	    			c=false;
	    		}
	    		if(xx==1 && animals.get(index).getEmplacement().getY()+1<10 )
	    		{  if(p[animals.get(index).getEmplacement().getX()][animals.get(index).getEmplacement().getY()+1].isB()==true)
	    			c=false;
	    		}
	    		if(xx==2 && animals.get(index).getEmplacement().getY()-1!=-1)
	    		{  if(p[animals.get(index).getEmplacement().getX()][animals.get(index).getEmplacement().getY()-1].isB()==true)
	    			c=false;
	    		}
	    		if(xx==3 && animals.get(index).getEmplacement().getX()-1!=-1)
	    		{  if(p[animals.get(index).getEmplacement().getX()-1][animals.get(index).getEmplacement().getY()].isB()==true)
	    			c=false;
	    		}
	    		if(xx==4 && animals.get(index).getEmplacement().getX()+1<10 )
	    		{  if(p[animals.get(index).getEmplacement().getX()+1][animals.get(index).getEmplacement().getY()].isB()==true)
	    			c=false;
	    		}
	     }finally{}

         }
        
         Animal anm= p[animals.get(index).getEmplacement().getX()][animals.get(index).getEmplacement().getY()].getAnimal();
         anm.setG_or_L(p[animals.get(index).getEmplacement().getX()][animals.get(index).getEmplacement().getY()].getAnimal().getG_or_L());
         p[animals.get(index).getEmplacement().getX()][animals.get(index).getEmplacement().getY()].setB(false);// la case n'est plus occup�e
         p[animals.get(index).getEmplacement().getX()][animals.get(index).getEmplacement().getY()].getAnimal().setYn(0);
        
         if(xx==1)
         {
         	animals.get(index).getEmplacement().setY(animals.get(index).getEmplacement().getY()+1);
         }
         if(xx==2)
         {
          	animals.get(index).getEmplacement().setY(animals.get(index).getEmplacement().getY()-1);

         }
         if(xx==3)
         {
          	animals.get(index).getEmplacement().setX(animals.get(index).getEmplacement().getX()-1);

         }
         if(xx==4)
         {
          	animals.get(index).getEmplacement().setX(animals.get(index).getEmplacement().getX()+1);

         }
         p[animals.get(index).getEmplacement().getX()][animals.get(index).getEmplacement().getY()].setB(true);// la case n'est plus occup�e
         p[animals.get(index).getEmplacement().getX()][animals.get(index).getEmplacement().getY()].setAnimal(anm);
         p[animals.get(index).getEmplacement().getX()][animals.get(index).getEmplacement().getY()].getAnimal().setYn(1);
      
	    }
	    
	   /**
	    * supprimer un animal
	    */
	    ///INPUT : les coordonn�es X et Y de l'animal � supprimer
	    public void supprimer(int x,int y)
	    {
	    	for(int i=0;i<animals.size();i++)
	    	{
	    		if(animals.get(i).getEmplacement().getX()==x && animals.get(i).getEmplacement().getY()==y)
	    		{
	    			animals.remove(i);
	    		}
	    	}
			this.setNombreAnimals(animals.size());   	
	    }
	    
	    
		/**
		 * Manger
		 */
	    public void manger()
	    {   
	    	this.VoirJeu();
	    	int exist=0,x=0,y=0;
	    	for(Animal animal: animals)
	    	{	animal.IncrementerAge();  
	    	    //mourir si age>30
	    		if(animal.getG_or_L()==2)// si c'�tait une gazelle
	    		{
	    			if(p[animal.getEmplacement().getX()][animal.getEmplacement().getY()].isHerbeDisponible()==true)
	    			{
	    				animal.setGain_from_food();//***********************khesk t ajouter mli ytsala l'herbe
	    				System.out.println("gazelle a mang� l'herbe");
	    			}
	    		}
	    		else
	    		if(animal.getG_or_L()==1)//si lion c'�tait un lion
	    		{  //on verra toutes les extr�mit�s 
	    			if(animal.getEmplacement().getX()<9 && animal.getEmplacement().getX()>0 && animal.getEmplacement().getY()<9 && animal.getEmplacement().getY()>0)// donc l'animal est au milieu du plateau
	    			{   //1 c�d il existe en haut, 2 il existe en bas,3 � droit 4 a gauche
	    				if(p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].isB()==true && p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].getAnimal().getG_or_L()==2)
	    				{
	    					p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].setB(false);
                            exist=1;
                            x=animal.getEmplacement().getX()-1;
                            y=animal.getEmplacement().getY();
	    					System.out.println("mang�");
	    					animal.setGain_from_food();
	    				}
	    				else 
	    				if(p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].isB()==true && p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].getAnimal().getG_or_L()==2)
	    				{
	    					p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].setB(false);
                            exist=1;
                            x=animal.getEmplacement().getX()+1;
                            y=animal.getEmplacement().getY();	    					System.out.println("mang�");
	    					animal.setGain_from_food();
	    				}
	    				else
	    				if(p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].isB()==true && p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].getAnimal().getG_or_L()==2)
	    				{
	    					p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].setB(false);
                            exist=1;
                            x=animal.getEmplacement().getX();
                            y=animal.getEmplacement().getY()-1;
	    					System.out.println("mang�");
	    					animal.setGain_from_food();
	    				}
	    				else 
	    				if(p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].isB()==true && p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].getAnimal().getG_or_L()==2)
	    				{
	    					p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].setB(false);
                            exist=1;
                            x=animal.getEmplacement().getX();
                            y=animal.getEmplacement().getY()+1;
	    					System.out.println("mang�");
	    					animal.setGain_from_food();
	    				}
	    			}
	    			
	    			
	    			if(animal.getEmplacement().getX()==0)
	    			{
	    				if(p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].isB()==true && p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].getAnimal().getG_or_L()==2)
	    				{
	    					p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].setB(false);
                            exist=1;
                            x=animal.getEmplacement().getX()+1;
                            y=animal.getEmplacement().getY();
	    					System.out.println("mang�");
	    					animal.setGain_from_food();
	    				}
	    				
	    				if(animal.getEmplacement().getY()!=0  )
	    				{
		    				if(p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].isB()==true && p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].getAnimal().getG_or_L()==2)
		    				{
		    					p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].setB(false);
	                            exist=1;
	                            x=animal.getEmplacement().getX();
	                            y=animal.getEmplacement().getY()-1;
		    					System.out.println("mang�");
		    					animal.setGain_from_food();
		    				}
	    				}
	    				if(animal.getEmplacement().getY()!=9)	
	    				{
	    					
	    					if(p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].isB()==true && p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].getAnimal().getG_or_L()==2)
		    				{
		    					p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].setB(false);
	                            exist=1;
	                            x=animal.getEmplacement().getX();
	                            y=animal.getEmplacement().getY()+1;
		    					System.out.println("mang�");
		    					animal.setGain_from_food();
		    				}

	    					
	    				}
	    			}
	    			
	    			if(animal.getEmplacement().getX()==9)
	    			{

	    				if(p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].isB()==true && p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].getAnimal().getG_or_L()==2)
	    				{
	    					p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].setB(false);
                            exist=1;
                            x=animal.getEmplacement().getX()-1;
                            y=animal.getEmplacement().getY();
	    					System.out.println("mang�");
	    					animal.setGain_from_food();
	    				}
	    				if(animal.getEmplacement().getY()!=0  )
	    				{
		    				if(p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].isB()==true && p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].getAnimal().getG_or_L()==2)
		    				{
		    					p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].setB(false);
	                            exist=1;
	                            x=animal.getEmplacement().getX();
	                            y=animal.getEmplacement().getY()-1;
		    					System.out.println("mang�");
		    					animal.setGain_from_food();
		    				}
	    				}
	    				if(animal.getEmplacement().getY()!=9)	
	    				{
	       					if(p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].isB()==true && p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].getAnimal().getG_or_L()==2)
		    				{
		    					p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].setB(false);
	                            exist=1;
	                            x=animal.getEmplacement().getX();
	                            y=animal.getEmplacement().getY()+1;
		    					System.out.println("mang�");
		    					animal.setGain_from_food();
		    				}

	    					
	    				}
	    				
	    			}
	    			if(animal.getEmplacement().getY()==0)
	    			{

	    				if(p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].isB()==true && p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].getAnimal().getG_or_L()==2)
	    				{
	    					p[animal.getEmplacement().getX()][animal.getEmplacement().getY()+1].setB(false);
                            exist=1;
                            x=animal.getEmplacement().getX();
                            y=animal.getEmplacement().getY()+1;
	    					System.out.println("mang�");
	    					animal.setGain_from_food();
	    				}
	    				if(animal.getEmplacement().getX()!=0  )
	    				{
		    				if(p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].isB()==true && p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].getAnimal().getG_or_L()==2)
		    				{
		    					p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].setB(false);
	                            exist=1;
	                            x=animal.getEmplacement().getX()-1;
	                            y=animal.getEmplacement().getY();
		    					System.out.println("mang�");
		    					animal.setGain_from_food();
		    				}
	    				}
	    				if(animal.getEmplacement().getX()!=9)	
	    				{
	       					if(p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].isB()==true && p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].getAnimal().getG_or_L()==2)
		    				{
		    					p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].setB(false);
	                            exist=1;
	                            x=animal.getEmplacement().getX()+1;
	                            y=animal.getEmplacement().getY();
		    					System.out.println("mang�");
		    					animal.setGain_from_food();
		    				}			
	    				}
	    				
	    			}
	    			if(animal.getEmplacement().getY()==9)
	    			{

	    				if(p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].isB()==true && p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].getAnimal().getG_or_L()==2)
	    				{
	    					p[animal.getEmplacement().getX()][animal.getEmplacement().getY()-1].setB(false);
                            exist=1;
                            x=animal.getEmplacement().getX();
                            y=animal.getEmplacement().getY()-1;
	    					System.out.println("mang�");
	    					animal.setGain_from_food();
	    				}
	    				if(animal.getEmplacement().getX()!=0  )
	    				{
		    				if(p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].isB()==true && p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].getAnimal().getG_or_L()==2)
		    				{
		    					p[animal.getEmplacement().getX()-1][animal.getEmplacement().getY()].setB(false);
	                            exist=1;
	                            x=animal.getEmplacement().getX()-1;
	                            y=animal.getEmplacement().getY();
		    					System.out.println("mang�");
		    					animal.setGain_from_food();
		    				}
	    				}
	    				if(animal.getEmplacement().getX()!=9)	
	    				{
	       					if(p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].isB()==true && p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].getAnimal().getG_or_L()==2)
		    				{
		    					p[animal.getEmplacement().getX()+1][animal.getEmplacement().getY()].setB(false);
	                            exist=1;
	                            x=animal.getEmplacement().getX()+1;
	                            y=animal.getEmplacement().getY();
		    					System.out.println("mang�");
		    					animal.setGain_from_food();
		    				}

	    					
	    				}
	    				
	    			}

	    			
	    		}
  
	    	}
  			if(exist==1)
			{
				this.supprimer(x, y);
			}
	    }



		/**
		 * se reproduire
		 */
	    public void sereproduire5()
        {  
	    	this.VoirJeu();
	    	
        	for(int i =0;i<animals.size();i++)
        	{ 
        		int Lion_OU_Gazelle=animals.get(i).getG_or_L();//1 si lion 2 si gazelle
        		for(int j=0;j<animals.size();j++)
        		{ 
        			boolean existe=false;
        			if(animals.get(i).getAge()>=10 && animals.get(j).getAge()>=10 && i!=j && animals.get(j).getG_or_L()==Lion_OU_Gazelle && animals.get(i).getGender()==1 && animals.get(j).getGender()==0)
        			{   
        				System.out.println("age :"+animals.get(i).getAge());
        				this.VoirJeu();

        				if(animals.get(j).getEmplacement().getX()!=9 && animals.get(j).getEmplacement().getY()!=9)
        				{
        					if(animals.get(i).getEmplacement().getX()==animals.get(j).getEmplacement().getX() && animals.get(i).getEmplacement().getY()==animals.get(j).getEmplacement().getY()-1)
        					{
        						existe=true;
        					}
        				  
        					if(animals.get(i).getEmplacement().getX()==animals.get(j).getEmplacement().getX() && animals.get(i).getEmplacement().getY()==animals.get(j).getEmplacement().getY()+1)
        					{
        						existe=true;
        					}
        				  
        					if(animals.get(i).getEmplacement().getX()==animals.get(j).getEmplacement().getX()-1 && animals.get(i).getEmplacement().getY()==animals.get(j).getEmplacement().getY())
        					{
        						existe=true;
        					}
        				  
        					if(animals.get(i).getEmplacement().getX()==animals.get(j).getEmplacement().getX()+1 && animals.get(i).getEmplacement().getY()==animals.get(j).getEmplacement().getY())
        					{
        						existe=true;
        					}
        				}
        			}
        			
        			if(existe==true)
        			{     
        				int gender;
        			    double x=(double)Math.random()*10;
        		  		if(x<5)
        		  		{
        		  			gender=1;
        		  		}
        		  		else
        		  		{
        		  			gender=0;
        		  		}
        	            //reproduction d'animal
        		    	if(Lion_OU_Gazelle==1)
        		    	{
        		    		Lion lion=new Lion();
        		    		lion.setG_or_L(Lion_OU_Gazelle);
        		    		lion.setGender(gender);
        		    		//Se positionner
        		    		do
        		    		{
        		    		   lion.Sepositionner();
        		    		}while(p[lion.getEmplacement().getX()][lion.getEmplacement().getY()].isB()==true);
        		    		this.ajouterAnimal(lion);
        		    		p[lion.getEmplacement().getX()][lion.getEmplacement().getY()].setB(true);
        		    	 }
        		    	 if(Lion_OU_Gazelle==2)
        		    	 {
        		    		 Gazelle gazelle=new Gazelle();
        		    		 gazelle.setG_or_L(Lion_OU_Gazelle);
        		    		 gazelle.setGender(gender);
        		    	     //Se positionner
        		    		 do
        		             {
        		    		   gazelle.Sepositionner();
        		    		 }while(p[gazelle.getEmplacement().getX()][gazelle.getEmplacement().getY()].isB()==true);
        		    		  this.ajouterAnimal(gazelle);
        		    		  p[gazelle.getEmplacement().getX()][gazelle.getEmplacement().getY()].setB(true);
        		    		  this.bouger(i);
        		    		  this.bouger(j);
        		    	 }
        			}
        		}
        	}
        }    
}
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

public class MyRecursiveAction extends RecursiveAction {

    private long workLoad = 0;
    private Plateau plt;

    public MyRecursiveAction(long workLoad,Plateau plt) {
        this.workLoad = workLoad;
        this.plt=plt;
    }

    @Override
    protected void compute() { 

        //if work is above threshold, break tasks up into smaller tasks
        if(this.workLoad > 0) {

            List<MyRecursiveAction> subtasks =
                new ArrayList<MyRecursiveAction>();

            subtasks.addAll(createSubtasks());

            for(RecursiveAction subtask : subtasks){
                subtask.fork();
            }

        }
    }

    private List<MyRecursiveAction> createSubtasks() {
        List<MyRecursiveAction> subtasks =
            new ArrayList<MyRecursiveAction>();
        plt.bouger((int)workLoad-1);
        MyRecursiveAction subtask1 = new MyRecursiveAction(this.workLoad-1,plt);
        plt.bouger((int)workLoad-1);

        MyRecursiveAction subtask2 = new MyRecursiveAction(this.workLoad-1,plt);
        invokeAll(subtask1,subtask2);

        subtasks.add(subtask1);
        this.plt.VoirJeu();
        subtasks.add(subtask2);
        this.plt.VoirJeu();

        return subtasks;
    }
}
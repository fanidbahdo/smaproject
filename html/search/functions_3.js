var searchData=
[
  ['getage_65',['getAge',['../class_gazelle.html#afa23c19ca8b9d5bec32cd413b5c00ac4',1,'Gazelle.getAge()'],['../class_lion.html#a40f623de14e397b537d1c62c9a7c6e08',1,'Lion.getAge()']]],
  ['getanimal_66',['getAnimal',['../class_plot.html#abcef619950615f116a1001ce79b337b0',1,'Plot']]],
  ['getemplacement_67',['getEmplacement',['../class_gazelle.html#a7144d3d84f52fe33f3414585705f1fcf',1,'Gazelle.getEmplacement()'],['../class_lion.html#a3ea0064209659cdf327f1dbe7bacbbb1',1,'Lion.getEmplacement()']]],
  ['getg_5for_5fl_68',['getG_or_L',['../class_gazelle.html#a48016d13d662be330aca9c27a30907a6',1,'Gazelle.getG_or_L()'],['../class_lion.html#a4204642dce1e0049172ad8a31186fe11',1,'Lion.getG_or_L()']]],
  ['getgain_5ffrom_5ffood_69',['getGain_from_food',['../class_gazelle.html#aa1fc744a1add7985682546bd549ab001',1,'Gazelle.getGain_from_food()'],['../class_lion.html#a8f5efa4e7a929fcc956c81e60ffd3244',1,'Lion.getGain_from_food()']]],
  ['getgender_70',['getGender',['../class_gazelle.html#ab48e3cd7bc2bf7a7793fa6799e495b21',1,'Gazelle.getGender()'],['../class_lion.html#ab6e17062e4b6b851278a6f153ffe67a9',1,'Lion.getGender()']]],
  ['getinstance_71',['getInstance',['../class_singleton_game.html#a7ab28a8494299df4935e132ca8bc4330',1,'SingletonGame']]],
  ['getnamezone_72',['getNameZone',['../class_plateau.html#a271d9e43d2fdab188b5c22e6dba6710c',1,'Plateau']]],
  ['getnomanimal_73',['getNomAnimal',['../class_plot.html#af21d0be9ae5d3a2e6aa7c06383548282',1,'Plot']]],
  ['getnombreanimals_74',['getNombreAnimals',['../class_plateau.html#affed22b1aa02be39fc91607bcaeee73d',1,'Plateau']]],
  ['getp_75',['getP',['../class_plateau.html#aa9951b045b31639477495a3db10ff7fa',1,'Plateau']]],
  ['getplateau_76',['getPlateau',['../class_singleton_game.html#acecb53524ba49d705843d305f914a4fc',1,'SingletonGame']]],
  ['getx_77',['getX',['../class_plot.html#a00afc2cb702f90dc878a37ac8fa7ef1e',1,'Plot']]],
  ['gety_78',['getY',['../class_plot.html#a89071cf8c5b69933f34420e93c10f830',1,'Plot']]]
];
